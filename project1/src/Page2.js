import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(imageName, size, recognition, download) {
  return {imageName, size, recognition, download };
}


const rows = [
  createData('Dog.png', '100x100' , 'dog', 'link'),
  createData('Cat.png', '100x100' , 'cat', 'link'),
  createData('Flower.png', '100x100' , 'flower', 'link'),
];

   export default function BasicTable() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 450}} aria-label="simple table">
        <TableHead>
          <TableRow>
          <TableCell>Imgae Name</TableCell>
            <TableCell align="center">Size</TableCell>
            <TableCell align="center">Recognition Result</TableCell>
            <TableCell align="center">Download link</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.imageName}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.imageName}
              </TableCell>
              <TableCell align="center">{row.size}</TableCell>
              <TableCell align="center">{row.recognition}</TableCell>
              <TableCell align="center">{row.download}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );}
  
     