import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(
  imageName: string,
  size: string,
  recognition: string,
  download: string,
) {
  return { imageName, size, recognition, download };
}

const rows = [
  createData('Dog.png', '100x100' , 'dog', 'link'),
  createData('Cat.png', '100x100' , 'cat', 'link'),
  createData('Flower.png', '100x100' , 'flower', 'link'),
];

export default function BasicTable() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Image Name</TableCell>
            <TableCell align="center">Size</TableCell>
            <TableCell align="center">Recognition Result</TableCell>
            <TableCell align="center">Download link</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.imageName}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.imageName}
              </TableCell>
              <TableCell align="right">{row.size}</TableCell>
              <TableCell align="right">{row.recognition}</TableCell>
              <TableCell align="right">{row.download}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}