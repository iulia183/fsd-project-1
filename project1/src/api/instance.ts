import axios from "axios";
import instance from "axios";


export const setAuthToken = token => {
    if (token) {
    instance.defaults.headers.common['Authorization'] = token;
    } 
    else {
    delete instance.defaults.headers.common['Authorization'];
    }
   }