import axios from "axios";

function instance ()
{
    const instance = axios.create({
    baseURL: 'http://localhost:3001',
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'}
  });

  return instance;
}