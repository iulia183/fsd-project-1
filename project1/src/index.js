import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Page2 from './Page2';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { DropzoneArea } from 'material-ui-dropzone';


ReactDOM.render(
  <React.StrictMode>
    < Page2/>
  </React.StrictMode>,
  document.getElementById('root')
);
const Index = () => {
  return (
    <div style={styles.app}>
      <Router>
        <Switch>
          <Route path="/App" exact>
            <App />
          </Route>

          <Route path="/page2" exact>
            <Page2 />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};
export default Index;
const styles = {
  app: {
    padding: 50
  }
}




// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
